use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rand::Rng; // Import Rng trait

async fn greet() -> impl Responder {
    HttpResponse::Ok().body("Hello, world!")
}

// Function to generate random numbers
async fn generate_random_numbers() -> impl Responder {
    let mut rng = rand::thread_rng();
    let numbers: Vec<f64> = (0..10).map(|_| rng.gen_range(0.0..1.0)).collect();
    HttpResponse::Ok().json(numbers) // Respond with JSON
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(greet))
            .route("/random", web::get().to(generate_random_numbers)) // New route for random numbers
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}

