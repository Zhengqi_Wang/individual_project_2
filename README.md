# Demo Video Link:
[![Demo Video Link](http://img.youtube.com/vi/QWZWUbB_zAQ/0.jpg)](http://www.youtube.com/watch?v=QWZWUbB_zAQ "Click to Watch!")




# Step 1: Develop a Simple REST API/Web Service in Rust
1. Set Up Rust Environment: install it from the official Rust website.
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
2. Create a New Rust Project: Use Cargo, Rust's package manager, to create a new project. In terminal, run:
```
cargo new rust_microservice
cd rust_microservice
```
3. Add Dependencies: Edit Cargo.toml file to include dependencies like warp or actix-web for creating web services and tokio for async runtime.
```
[dependencies]
actix-web = "4.0"
```
4. Develop the API: Write Rust code to create a simple REST API. This could involve setting up routes, handling requests and responses, and integrating business logic. In a new terminal, we run 'cargo run' in terminal from the project's root directory.
We edit the main.rs file as:
```
use actix_web::{web, App, HttpResponse, HttpServer, Responder};

async fn greet() -> impl Responder {
    HttpResponse::Ok().body("Hello, world!")
}

// Function to generate random numbers
async fn generate_random_numbers() -> impl Responder {
    let mut rng = rand::thread_rng();
    let numbers: Vec<f64> = (0..10).map(|_| rng.gen_range(0.0..1.0)).collect();
    HttpResponse::Ok().json(numbers) // Respond with JSON
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(greet))
            .route("/random", web::get().to(generate_random_numbers)) // New route for random numbers
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
```
5. Test Locally: Ensure service runs correctly on local machine. Test it by making API calls (using tools like curl or Postman).
```
(base) supergeorge@wangzhegqideMBP rust_microservice % curl http://localhost:8080
Hello, world!%  
```
![Local testing: Hello Wrold](./images/Local_HelloWorld.png)

```
$ curl http://localhost:8080/random
[0.234, 0.567, 0.891, 0.123, 0.456, 0.789, 0.012, 0.345, 0.678, 0.901]
```

![Local random numbers](./images/Local_Random_Number.png)

# Step 2: Dockerfile to containerize service
1. Create a Dockerfile
create a new file named Dockerfile in the root directory of Rust project. Setting the dockerfile as:
```
FROM rust:latest as builder
WORKDIR /usr/src/myapp
COPY . .
RUN cargo build --release
RUN find target/release -type f -executable -exec cp {} myapp \;

FROM debian:buster-slim
COPY --from=builder /usr/src/myapp/myapp /usr/local/bin/myapp
CMD ["myapp"]
```
2. Build and Run Your Docker Container
First build the docker image with command of:
```
docker build -t my-rust-app .
```
We switch towards the Docker, and will see that
![Docker Images](./images/Docker_Images.png)
Second, we can run the Docker Container by command of:
```
docker run --name my-running-app my-rust-app
```
we can see on docker container as:
![Docker Containers](./images/Docker_Containers.png)

By following these steps, we can have Rust microservice containerized using Docker. 



# Step 3: CI/CD pipeline files with GitLab
1. Create a file named .gitlab-ci.yml
We need to define stages in pipeline, such as build, test, and deploy.
The '.gitlab-ci.yml' file is like:
```
stages:
  - build
  - test

build_job:
  stage: build
  image: rust:latest
  script:
    - cargo build --verbose --all

test_job:
  stage: test
  image: rust:latest
  script:
    - cargo test --verbose --all
```

2. After created and saved the .gitlab-ci.yml file, push it to GitLab repository.
```
git add .gitlab-ci.yml
git commit -m "Add GitLab CI/CD configuration"
git push origin master
```

3. Check the Pipeline Execution
Go to your GitLab project page and we can check on the pipelines pages.
![Pipeline](./images/Pipeline.png)

4. Local Host
We can check the local host as below. 
Hello world function:
![Local host for hello world function](./images/LocalHost_Helloworld.png)

Generate random numbers:
![Local host for hello world function](./images/LocalHost_Random.png)