FROM rust:latest as builder
WORKDIR /usr/src/myapp
COPY . .
RUN cargo build --release
RUN find target/release -type f -executable -exec cp {} myapp \;

FROM debian:buster-slim
COPY --from=builder /usr/src/myapp/myapp /usr/local/bin/myapp
CMD ["myapp"]
